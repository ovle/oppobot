-- todo should rely on autocreating tables by entity definitions, but what to do with the view?

CREATE TABLE IF NOT EXISTS player
(
    id uuid NOT NULL,
    ext_id bigint NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT player_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS game_round
(
    id uuid NOT NULL,
    ai_action character varying(255) COLLATE pg_catalog."default" NOT NULL,
    player_action character varying(255) COLLATE pg_catalog."default" NOT NULL,
    result character varying(255) COLLATE pg_catalog."default" NOT NULL,
    player_id uuid NOT NULL,
    CONSTRAINT game_round_pkey PRIMARY KEY (id),
    CONSTRAINT fkjbmgy4pwdotc4c2lu9uyaxmny FOREIGN KEY (player_id)
        REFERENCES public.player (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE OR REPLACE VIEW player_history AS
    SELECT
        gr.*,
        p.name as player_name,
        cast(win_results as decimal) / total as win_rate,
        cast(draw_results as decimal) / total as draw_rate,
        cast(lose_results as decimal) / total as lose_rate
    FROM (
        SELECT
            player_id,
            count(1) as total,
            count(1) filter (where result = 'WIN') as win_results,
            count(1) filter (where result = 'DRAW') as draw_results,
            count(1) filter (where result = 'LOSE') as lose_results,
            count(1) filter (where player_action = 'ROCK') as rock_actions,
            count(1) filter (where player_action = 'PAPER') as paper_actions,
            count(1) filter (where player_action = 'SCISSORS') as scissors_actions
        FROM game_round
        GROUP BY player_id
    ) gr join player p on p.id = gr.player_id;