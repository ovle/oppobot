package com.ovle.telebot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
//@EnableAutoConfiguration(exclude = [DataSourceAutoConfiguration::class, HibernateJpaAutoConfiguration::class])
class TelebotApplication

fun main(args: Array<String>) {
	runApplication<TelebotApplication>(*args)
}