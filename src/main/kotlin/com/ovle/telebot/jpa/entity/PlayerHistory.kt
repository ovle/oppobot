package com.ovle.telebot.jpa.entity

import org.hibernate.annotations.Immutable
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Immutable
@Table(name = "player_history")

class PlayerHistory(
    @Id
    @Column(nullable = false)
    val playerId: UUID,
    @Column(nullable = false)
    val playerName: String,

    @Column(nullable = false)
    val winRate: Float,
    @Column(nullable = false)
    val drawRate: Float,
    @Column(nullable = false)
    val loseRate: Float,

    @Column(nullable = false)
    val total: Int,

    @Column(nullable = false)
    val winResults: Int,
    @Column(nullable = false)
    val drawResults: Int,
    @Column(nullable = false)
    val loseResults: Int,

    @Column(nullable = false)
    val rockActions: Int,
    @Column(nullable = false)
    val paperActions: Int,
    @Column(nullable = false)
    val scissorsActions: Int,
)