package com.ovle.telebot.jpa.entity

import com.ovle.telebot.game.RPSAction
import com.ovle.telebot.game.RPSRoundResult
import java.util.*
import javax.persistence.*

@Entity
class GameRound(
    @Id
    val id: UUID,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "player_id", nullable = false)
    val player: Player,

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    val playerAction: RPSAction,

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    val aiAction: RPSAction,

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    val result: RPSRoundResult
)