package com.ovle.telebot.jpa.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Player(
    @Id
    @Column(nullable = false)
    val id: UUID,

    val extId: Long,

    @Column(nullable = false)
    val name: String
)