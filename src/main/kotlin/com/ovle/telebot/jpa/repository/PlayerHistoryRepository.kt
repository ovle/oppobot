package com.ovle.telebot.jpa.repository

import com.ovle.telebot.jpa.entity.PlayerHistory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PlayerHistoryRepository : JpaRepository<PlayerHistory, UUID> {
    fun findByPlayerId(playerId: UUID): PlayerHistory?
}