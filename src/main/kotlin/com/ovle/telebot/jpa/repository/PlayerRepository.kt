package com.ovle.telebot.jpa.repository

import com.ovle.telebot.jpa.entity.Player
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PlayerRepository : JpaRepository<Player, UUID> {

    fun findByExtId(id: Long): Player?
}