package com.ovle.telebot.jpa.repository

import com.ovle.telebot.jpa.entity.GameRound
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface GameRoundRepository : JpaRepository<GameRound, UUID>