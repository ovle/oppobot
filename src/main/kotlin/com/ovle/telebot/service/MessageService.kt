package com.ovle.telebot.service

import com.ovle.telebot.jpa.entity.Player
import com.ovle.telebot.jpa.repository.PlayerRepository
import com.ovle.telebot.service.command.CommandProcessor
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.User
import java.util.*


@Component
class MessageService(
    val playerRepository: PlayerRepository,
    val commandProcessors: Collection<CommandProcessor>
) {

    fun process(message: Message): ProcessResult? {
        val user = message.from
        val player = playerRepository.findByExtId(user.id) ?: createPlayer(user)

        return when {
          message.hasText() -> commandProcessors.find { it.accept(message) }?.process(message, player)
          else -> null
        }
    }

    private fun createPlayer(user: User) =
        Player(
            id = UUID.randomUUID(),
            extId = user.id,
            name = user.userName ?: user.firstName
        ).also {
            playerRepository.save(it)
        }
}