package com.ovle.telebot.service

data class ProcessResult(
    val responseText: String = "",
    val isReply: Boolean = false,
    val buttons: Collection<String> = listOf()
)