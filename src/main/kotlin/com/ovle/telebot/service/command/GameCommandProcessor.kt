package com.ovle.telebot.service.command

import com.ovle.telebot.game.GameEngine
import com.ovle.telebot.game.RPSAction
import com.ovle.telebot.jpa.entity.GameRound
import com.ovle.telebot.jpa.entity.Player
import com.ovle.telebot.service.ProcessResult
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message


private val commands = RPSAction.values().map { it.action }.toTypedArray()

@Component
class GameCommandProcessor(
    val gameEngine: GameEngine,
): CommandProcessor(commands) {

    override fun process(message: Message, player: Player): ProcessResult {
        val playerOption = RPSAction.values().find { it.action == message.text }!!

        val gameTurn = gameEngine.process(player, playerOption)
        val responseText = responseText(gameTurn)
        return ProcessResult(responseText)
    }

    private fun responseText(gameTurn: GameRound) =
        "${gameTurn.aiAction.action.uppercase()}. ${gameTurn.result.messages.random()}!"

}