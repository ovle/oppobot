package com.ovle.telebot.service.command

import com.ovle.telebot.game.RPSRoundResult
import com.ovle.telebot.jpa.entity.Player
import com.ovle.telebot.jpa.entity.PlayerHistory
import com.ovle.telebot.jpa.repository.PlayerHistoryRepository
import com.ovle.telebot.service.ProcessResult
import com.ovle.telebot.util.toPercent
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message


@Component
class ShowPlayerStatCommandProcessor(
    val playerHistoryRepository: PlayerHistoryRepository,
) : CommandProcessor(arrayOf("/stat")) {

    override fun process(message: Message, player: Player): ProcessResult {
        val playerHistory = playerHistoryRepository.findByPlayerId(player.id)
            ?: return ProcessResult("Статистика игрока не найдена")

        return ProcessResult(
            """
                    Игрок ${player.name}:
                    |Игр: ${playerHistory.total}
                    |${RPSRoundResult.values().map { gamesHistory(playerHistory, it) }}
                """.trimMargin(),
        )

    }

    private fun gamesHistory(playerHistory: PlayerHistory, result: RPSRoundResult) =
        """${result.description}: ${result.count(playerHistory)} (${result.rate(playerHistory).toPercent()}%)"""
}