package com.ovle.telebot.service.command

import com.ovle.telebot.jpa.entity.Player
import com.ovle.telebot.service.ProcessResult
import org.telegram.telegrambots.meta.api.objects.Message


abstract class CommandProcessor(private val commands: Array<String>) {

    fun accept(message: Message) = message.hasText()
        && message.text.lowercase() in commands.map { it.lowercase() }

    abstract fun process(message: Message, player: Player): ProcessResult
}

