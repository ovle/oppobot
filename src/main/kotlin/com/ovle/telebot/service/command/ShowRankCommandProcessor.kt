package com.ovle.telebot.service.command

import com.ovle.telebot.jpa.entity.Player
import com.ovle.telebot.jpa.entity.PlayerHistory
import com.ovle.telebot.jpa.repository.PlayerHistoryRepository
import com.ovle.telebot.service.ProcessResult
import com.ovle.telebot.util.toPercent
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message


@Component
class ShowRankCommandProcessor(
    val playerHistoryRepository: PlayerHistoryRepository,
) : CommandProcessor(arrayOf("/rank")) {

    companion object {
        private const val MIN_RANK_GAMES = 10
        private const val PLAYERS_TO_DISPLAY = 10
    }

    override fun process(message: Message, player: Player): ProcessResult {
        val sort = Sort.by("winRate", "winResults").descending()
        val playersHistory = playerHistoryRepository.findAll(sort)
            .filter { it.total >= MIN_RANK_GAMES }
            .take(PLAYERS_TO_DISPLAY)

        if (playersHistory.isEmpty()) return ProcessResult("Недостаточно истории по игрокам")

        val playersHistoryInfo = playersHistory
            .mapIndexed { index, playerHistory -> playerHistory(index, playerHistory) }
            .joinToString("\n")

        return ProcessResult(
            """
                |Лидеры (минимум $MIN_RANK_GAMES игр):
                |$playersHistoryInfo
            """.trimMargin(),
        )
    }

    private fun playerHistory(index: Int, playerHistory: PlayerHistory) =
        """ ${index + 1}. ${playerHistory.playerName}: ${playerHistory.winResults} побед (${playerHistory.winRate.toPercent()}%)"""
            .trimMargin()
}