package com.ovle.telebot.service.command

import com.ovle.telebot.game.RPSAction
import com.ovle.telebot.jpa.entity.Player
import com.ovle.telebot.service.ProcessResult
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message

@Component
class HelpCommandProcessor: CommandProcessor(arrayOf("/help")) {

    override fun process(message: Message, player: Player): ProcessResult {
        return ProcessResult(
            """
                |Привет, ${player.name}!
                |Я - бот, со мной можно поиграть в камень-ножницы-бумагу.
                |Команды:
                |/help - помощь
                |/stat - статистика игрока
                |/rank - топ игроков
            """.trimMargin(),
            buttons = RPSAction.values().map(RPSAction::action)
        )
    }
}