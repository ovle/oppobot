package com.ovle.telebot

import com.ovle.telebot.service.MessageService
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow

@Component
class Bot(val messageService: MessageService): TelegramLongPollingBot() {

    @Value("\${bot.username}")
    private val botUsername: String = ""
    override fun getBotUsername() = botUsername

    @Value("\${bot.token}")
    private val botToken: String = ""
    override fun getBotToken() = botToken


    override fun onUpdateReceived(update: Update) {
        if (update.hasMessage()) {
            val message = update.message
            val result = messageService.process(message) ?: return
            val replyMessageId = if (result.isReply) message.messageId else null

            reply(message.chatId.toString(), result.responseText, replyMessageId, result.buttons)
        }
    }

    private fun reply(chatId: String, responseText: String, replyMessageId: Int?, buttons: Collection<String>) {
        if (responseText.isEmpty()) return

        val responseMessage = SendMessage(chatId, responseText).apply {
            parseMode = "Markdown"
            replyToMessageId = replyMessageId
            replyMarkup = getReplyMarkup(buttons)
        }

        execute(responseMessage)
    }

    private fun getReplyMarkup(buttons: Collection<String>): ReplyKeyboard? {
        val result = ReplyKeyboardMarkup()
        val row = KeyboardRow()
        buttons.forEach { row.add(it) }
        result.keyboard = listOf(row)

        return result
    }
}