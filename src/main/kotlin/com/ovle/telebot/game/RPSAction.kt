package com.ovle.telebot.game

enum class RPSAction(val action: String) {
    ROCK("камень"),
    SCISSORS("ножницы"),
    PAPER("бумага");

    fun beats() = when(this) {
        ROCK -> arrayOf(SCISSORS)
        SCISSORS -> arrayOf(PAPER)
        PAPER -> arrayOf(ROCK)
    }
}

