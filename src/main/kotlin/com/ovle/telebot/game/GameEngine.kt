package com.ovle.telebot.game

import com.ovle.telebot.game.RPSRoundResult.*
import com.ovle.telebot.game.strategy.GameStrategy
import com.ovle.telebot.jpa.entity.GameRound
import com.ovle.telebot.jpa.entity.Player
import com.ovle.telebot.jpa.repository.GameRoundRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class GameEngine(
    val gameRoundRepository: GameRoundRepository,
    val gameStrategies: Collection<GameStrategy>
) {

    fun process(player: Player, playerAction: RPSAction): GameRound {
        val aiAction = selectStrategy(player)?.getAction(player)
            ?: throw IllegalStateException("no game strategies found")
        val result = result(playerAction, aiAction)

        val gameRound = GameRound(
            id = UUID.randomUUID(),
            player = player,
            playerAction = playerAction,
            aiAction = aiAction,
            result = result
        )

        gameRoundRepository.save(gameRound)

        return gameRound
    }

    //todo choose by condition
    private fun selectStrategy(player: Player) = gameStrategies.randomOrNull()

    private fun result(playerAction: RPSAction, aiAction: RPSAction) = when {
        aiAction in playerAction.beats() -> WIN
        playerAction in aiAction.beats() -> LOSE
        else -> DRAW
    }
}