package com.ovle.telebot.game

import com.ovle.telebot.jpa.entity.PlayerHistory

enum class RPSRoundResult(val description: String, vararg val messages: String) {
    WIN(
        description = "победа",
        "ты победил(а), но осмелишься ли ты сразиться снова?",
        "победа? тут где-то, наверное, ошибка. давай ещё раз",
        "что ж, новичкам везёт"
    ) {
        override fun count(ph: PlayerHistory) = ph.winResults
        override fun rate(ph: PlayerHistory) = ph.winRate
    },
    DRAW(
        description = "ничья",
        "ничья. бессмысленно потраченное время",
        "ничья лучше, чем поражение, правда?"
    ) {
        override fun count(ph: PlayerHistory) = ph.drawResults
        override fun rate(ph: PlayerHistory) = ph.drawRate

    },
    LOSE(
        description = "поражение",
        "поражение. ну, этого следовало ожидать",
        "очередное поражение",
        "может, тебе стоит заняться чем-то другим?"
    ) {
        override fun count(ph: PlayerHistory) = ph.loseResults
        override fun rate(ph: PlayerHistory) = ph.loseRate
    };

    abstract fun count(ph: PlayerHistory): Int
    abstract fun rate(ph: PlayerHistory): Float
}