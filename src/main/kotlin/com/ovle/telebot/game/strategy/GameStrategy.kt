package com.ovle.telebot.game.strategy

import com.ovle.telebot.game.RPSAction
import com.ovle.telebot.jpa.entity.Player

interface GameStrategy {
    fun getAction(opponent: Player): RPSAction
}