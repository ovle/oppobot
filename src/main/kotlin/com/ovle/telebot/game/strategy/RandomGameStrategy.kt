package com.ovle.telebot.game.strategy

import com.ovle.telebot.game.RPSAction
import com.ovle.telebot.jpa.entity.Player
import org.springframework.stereotype.Component

@Component
class RandomGameStrategy: GameStrategy {
    override fun getAction(opponent: Player) = RPSAction.values().random()
}