package com.ovle.telebot.util

import kotlin.math.roundToInt

fun Float.toPercent(): Int = (this * 100).roundToInt()